package za.co.sb.devserver.clients.feature;

import javax.enterprise.context.Dependent;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 *
 * @author steven
 */
@RegisterRestClient
//@Dependent
public interface HealthClient {
    
    @GET
    @Path("/health")          
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(description = "This is the health check method that will be called on your service")
    public Response health();
}
