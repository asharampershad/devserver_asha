/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.rest.vo;

import java.util.List;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author steven
 */
public class RegisterListVO {
    
    @Schema(description = "List of Registered servers")
    private List<ServiceVO> serversList;

    public List<ServiceVO> getServersList() {
        return serversList;
    }

    public void setServersList(List<ServiceVO> serversList) {
        this.serversList = serversList;
    }
    
    
}
