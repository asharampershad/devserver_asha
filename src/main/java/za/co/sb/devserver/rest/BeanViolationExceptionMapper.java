package za.co.sb.devserver.rest;

import java.util.Arrays;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import za.co.sb.devserver.rest.vo.RegisterResultVO;

/**
 * Creates a response object for validations on the input objects
 * @author steven
 */
@Provider
public class BeanViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException e) {        
        RegisterResultVO regRes = new RegisterResultVO(false);
        String[] errors = new String[e.getConstraintViolations().size()];
        int x = 0;
        for (ConstraintViolation conViol :e.getConstraintViolations()) {
            String propertyPath = conViol.getPropertyPath().toString();
            errors[x] = conViol.getMessage() + " in property " + propertyPath.substring(propertyPath.lastIndexOf('.') + 1);
            x++;            
        }
        Arrays.sort(errors);
        regRes.setErrors(errors);
        return Response.status(Response.Status.BAD_REQUEST).entity(regRes).build();
    }

}
