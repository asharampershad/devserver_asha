/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.rest.vo;

/**
 *
 * @author steven
 */
public class RegisterResultVO {
    
    boolean registered;
    String[] errors;

    public RegisterResultVO() {
        
    }
    
    public RegisterResultVO(boolean registered, String[]errors) {
        this.registered = registered;
        this.errors = errors;
    }
    
    public RegisterResultVO(boolean registered) {
        this.registered = registered;
    }
    
    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public String[] getErrors() {
        return errors;
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }
    
    
}
