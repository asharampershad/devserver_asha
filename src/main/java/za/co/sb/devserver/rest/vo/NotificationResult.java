package za.co.sb.devserver.rest.vo;

/**
 *
 * @author steven
 */
public class NotificationResult {
    
    private String serverName;
    private boolean successful;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }   

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
    
    
}
