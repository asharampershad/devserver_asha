package za.co.sb.devserver.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author steven
 */
@ApplicationPath("devservice")
public class DevServiceApplication extends Application {
    
}
